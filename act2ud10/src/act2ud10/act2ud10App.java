package act2ud10;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

import dto.act2ud10Excepcio;



public class act2ud10App {

	public static void main(String[] args) {
		
		try {
			String mostrar = "Mensaje mostrado por pantalla";
			System.out.println(mostrar);
			if(mostrar!="act2ud10") {
				throw new act2ud10Excepcio();
			}
		} catch (act2ud10Excepcio laMevaExepcioCustomitzada) {
			
			System.out.println("Excepcion capturada con mensaje: " + laMevaExepcioCustomitzada.getMessage());
			
		} finally {
			System.out.println("Programa terminado");
		}
		
	}


}
